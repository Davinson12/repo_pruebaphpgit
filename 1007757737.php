<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>1007757737</title>
</head>
<body>

<form action="1007757737.php" method="get">
  <p>Nombre: <input type="text" name="nombre" size="40" required></p>
  <p>Fecha de nacimiento: <input type="date" name="nacido" required></p>
  <p>
    <input type="submit" value="Enviar">
    <input type="reset" value="Borrar">
  </p>
</form>

<?php 
extract($_GET);
date_default_timezone_set('America/Bogota');

if (isset($nombre)) {
    echo $nombre;
    echo "<br><br>";
    echo "Edad: ";

    $cumpleanos = new DateTime($nacido);
    $hoy = new DateTime();
    $annos = $hoy->diff($cumpleanos);
    echo $annos->y;
    echo " años";

    echo "<br><br>";

    echo "Años impares: ";
    echo "<p style='border-image: initial; border: 1px solid blue; width: 100px;'>";

    $fecha = $nacido;
    $fechaComoEntero = strtotime($fecha);
    $anio = date("Y", $fechaComoEntero);
    $hoy2 = Date("Y");

    $num1=$anio;
    $num2=$hoy2;
    for($i=$num1; $i<=$num2; $i++){
        if( $i%2 != 0 ){
            echo $i.'<br />';
        }
    }

    echo "</p>";

}

?>

</body>
</html>